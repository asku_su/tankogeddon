

#include "TGProjectile.h"


ATGProjectile::ATGProjectile()
{
	PrimaryActorTick.bCanEverTick = true;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>("ProjectileMesh");
	SetRootComponent(ProjectileMesh);
	
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComponent");
}

void ATGProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATGProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

