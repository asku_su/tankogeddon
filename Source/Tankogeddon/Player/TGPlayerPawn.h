
#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "Tankogeddon/Cannons/TGCannon.h"
#include "TGPlayerPawn.generated.h"

class ATGPlayerController;

UCLASS()
class TANKOGEDDON_API ATGPlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	ATGPlayerPawn();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void MoveForvard(float AxisValue);

	UFUNCTION()
	void MoveRight(float AxisValue);

	UFUNCTION()
	void SetCannon(TSubclassOf<ATGCannon> CannonClass, ECannonType CannonType, int CountOfShells);

	UFUNCTION()
	void RefillAmmoAllCannons(int count);

	UFUNCTION()
	void StartMainFire();
	UFUNCTION()
	void StopMainFire() const;

	UFUNCTION()
	void StartSecondFire();
	UFUNCTION()
	void StopSecondFire() const;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* BodyMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* TurretMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	USpringArmComponent* SpringArmComponent; 

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UCameraComponent* FollowCamera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Debug")
	bool NeedDrawDebug = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UArrowComponent* MainCannonPivotComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UArrowComponent* SecondCannonPivotComponent; 
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement|Speed")
	float MoveSpeed = 300.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement|Acceleration")
	float MoveAcceleration = 300.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement|Acceleration")
	float ResistanceMoveAcceleration = 500.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement|Speed")
	float RotationSpeed = 100;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement|Acceleration")
	float RotationAcceleration = 50.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement|Acceleration")
	float ResistanceRotationAcceleration = 150.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Turret")
	float TurretRotationSpeed = 100;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cannons")
	TSubclassOf<ATGCannon> DefaultMainCannonClass;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cannons")
	TSubclassOf<ATGCannon> DefaultSecondCannonClass;

	UPROPERTY()
	ATGPlayerController* PlayerController;

protected:
	virtual void BeginPlay() override;

	void PerformMovement(float DeltaTime);
	void PerformTurretRotation(float DeltaTime) const;
	
	ATGCannon* SpawnCannon(TSubclassOf<ATGCannon> CannonClass, UArrowComponent* PivotComponent, int CountOfShells = 0) const;
	
private:
	UPROPERTY()
	ATGCannon* MainCannon;
	UPROPERTY()
	ATGCannon* SecondCannon;
	
	float CurrentMoveSpeed = 0.f;
	float CurrentRotationSpeed = 0.f;
		
	float TargetForvardAxisValue;
	float TargetRightAxisValue;
};
