
#include "TGPlayerController.h"

#include "DrawDebugHelpers.h"

ATGPlayerController::ATGPlayerController()
{
	bShowMouseCursor = true;
}

void ATGPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForvard", this, &ATGPlayerController::OnMoveForvardInput);
	InputComponent->BindAxis("MoveRight", this, &ATGPlayerController::OnMoveRightInput);

	InputComponent->BindAction("MainFire", EInputEvent::IE_Pressed, this, &ATGPlayerController::OnClickMainFire);
	InputComponent->BindAction("MainFire", EInputEvent::IE_Released, this, &ATGPlayerController::OnReleasedMainFire);
	InputComponent->BindAction("SecondFire", EInputEvent::IE_Pressed, this, &ATGPlayerController::OnClickSecondFire);		
	InputComponent->BindAction("SecondFire", EInputEvent::IE_Released, this, &ATGPlayerController::OnReleasedSecondFire);		
}

void ATGPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CalculateMousePositionNearPawn();
}

FVector ATGPlayerController::GetMousePosition()
{
	return MousePosition;
}

void ATGPlayerController::BeginPlay()
{
	Super::BeginPlay();

	PlayerPawn = Cast<ATGPlayerPawn>(GetPawn());
}

void ATGPlayerController::OnMoveForvardInput(float AxisValue)
{
	PlayerPawn->MoveForvard(AxisValue);
}

void ATGPlayerController::OnMoveRightInput(float AxisValue)
{
	PlayerPawn->MoveRight(AxisValue);
}

void ATGPlayerController::CalculateMousePositionNearPawn()
{	
	FVector mouseDirection;
	DeprojectMousePositionToWorld(MousePosition, mouseDirection);
	
	FVector pawnLocation = PlayerPawn->GetActorLocation();
	FVector direction = MousePosition - PlayerPawn->FollowCamera->GetComponentLocation();
	direction.Normalize();
	MousePosition += direction * 1000.f;
	MousePosition.Z = pawnLocation.Z;

	if (PlayerPawn->NeedDrawDebug)
	{
		DrawDebugLine(GetWorld(), pawnLocation, MousePosition, FColor::Green);
	}
}

void ATGPlayerController::OnClickMainFire()
{
	PlayerPawn->StartMainFire();
}

void ATGPlayerController::OnReleasedMainFire()
{
	PlayerPawn->StopMainFire();
}

void ATGPlayerController::OnClickSecondFire()
{
	PlayerPawn->StartSecondFire();
}

void ATGPlayerController::OnReleasedSecondFire()
{
	PlayerPawn->StopSecondFire();
}
