
#include "TGPlayerPawn.h"

#include "DrawDebugHelpers.h"
#include "TGPlayerController.h"
#include "Kismet/KismetMathLibrary.h"
#include "Tankogeddon/Tankogeddon.h"

ATGPlayerPawn::ATGPlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	BodyMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("BodyMeshComponent");
	SetRootComponent(BodyMeshComponent);

	TurretMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("TurretMeshComponent");
	TurretMeshComponent->SetupAttachment(BodyMeshComponent);

	MainCannonPivotComponent = CreateDefaultSubobject<UArrowComponent>("MainCannonPivotComponent");
	MainCannonPivotComponent->SetupAttachment(TurretMeshComponent);

	SecondCannonPivotComponent = CreateDefaultSubobject<UArrowComponent>("SecondCannonPivotComponent");
	SecondCannonPivotComponent->SetupAttachment(TurretMeshComponent);
	
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent"); 
	SpringArmComponent->SetupAttachment(BodyMeshComponent);
	
	FollowCamera = CreateDefaultSubobject<UCameraComponent>("FollowCamera");
	FollowCamera->SetupAttachment(SpringArmComponent);
}

void ATGPlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = Cast<ATGPlayerController>(GetController());
	
	MainCannon = SpawnCannon(DefaultMainCannonClass, MainCannonPivotComponent);
	SecondCannon = SpawnCannon(DefaultSecondCannonClass, SecondCannonPivotComponent);
}

void ATGPlayerPawn::PerformMovement(float DeltaTime)
{
	const FVector currentLocation = GetActorLocation();
	const FVector forvardLocation = GetActorForwardVector();

	float resultAcceleration = 0.f;
	if (FMath::IsNearlyZero(TargetForvardAxisValue))
	{
		resultAcceleration = ResistanceMoveAcceleration;
	}
	else if (FMath::Sign(TargetForvardAxisValue) == FMath::Sign(CurrentMoveSpeed))
	{
		resultAcceleration = MoveAcceleration;
	}
	else
	{
		resultAcceleration = MoveAcceleration + ResistanceMoveAcceleration;
	}	
	CurrentMoveSpeed = FMath::FInterpConstantTo(CurrentMoveSpeed, MoveSpeed * TargetForvardAxisValue, DeltaTime, resultAcceleration);
	const FVector movePosition = currentLocation + forvardLocation * CurrentMoveSpeed * DeltaTime;
	SetActorLocation(movePosition, true);

	const FRotator currentRotation = GetActorRotation();		
	
	if (FMath::IsNearlyZero(TargetRightAxisValue))
	{
		resultAcceleration = ResistanceRotationAcceleration;
	}
	else if (FMath::Sign(TargetRightAxisValue) == FMath::Sign(CurrentRotationSpeed))
	{
		resultAcceleration = RotationAcceleration;
	}
	else
	{
		resultAcceleration = RotationAcceleration + ResistanceRotationAcceleration;
	}	
	
	CurrentRotationSpeed = FMath::FInterpConstantTo(CurrentRotationSpeed, RotationSpeed * TargetRightAxisValue, DeltaTime, resultAcceleration);
	const FRotator moveRotation = currentRotation + FRotator(0.f, CurrentRotationSpeed * DeltaTime ,0.f);
	SetActorRotation(moveRotation);
}

void ATGPlayerPawn::PerformTurretRotation(float DeltaTime) const
{
	FRotator newTargetTurretRotation = UKismetMathLibrary::FindLookAtRotation(TurretMeshComponent->GetComponentLocation(), PlayerController->GetMousePosition());
	newTargetTurretRotation.Pitch = TurretMeshComponent->GetComponentRotation().Pitch;
	newTargetTurretRotation.Roll = TurretMeshComponent->GetComponentRotation().Roll;	
	TurretMeshComponent->SetWorldRotation(FMath::Lerp(TurretMeshComponent->GetComponentRotation(), newTargetTurretRotation,TurretRotationSpeed / 1000.f));
}


ATGCannon* ATGPlayerPawn::SpawnCannon(TSubclassOf<ATGCannon> CannonClass, UArrowComponent* PivotComponent, int CountOfShells) const
{
	if (CannonClass != nullptr)
	{
		FActorSpawnParameters spawnParameters;
		spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ATGCannon* Cannon = GetWorld()->SpawnActor<ATGCannon>(CannonClass, PivotComponent->GetComponentLocation(), PivotComponent->GetComponentRotation(), spawnParameters);
		Cannon->AttachToComponent(PivotComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		Cannon->RefillAmmo(CountOfShells);
		return Cannon;
	}
	else
	{
		return nullptr;
	}
}

void ATGPlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	PerformMovement(DeltaTime);
	PerformTurretRotation(DeltaTime);
}

void ATGPlayerPawn::MoveForvard(float AxisValue)
{
	TargetForvardAxisValue = AxisValue;
}

void ATGPlayerPawn::MoveRight(float AxisValue)
{
	TargetRightAxisValue = AxisValue;
}

void ATGPlayerPawn::SetCannon(TSubclassOf<ATGCannon> CannonClass, ECannonType CannonType, int CountOfShells)
{
	if(CannonClass)
	{
		switch (CannonType)
		{
		case ECannonType::Unknown_Cannon : break;
		case ECannonType::Main_Cannon :
			if (MainCannon)
			{
				MainCannon->Destroy();
			}
			MainCannon = SpawnCannon(CannonClass, MainCannonPivotComponent);
			break;
		case ECannonType::Second_Cannon :
			if (SecondCannon)
			{
				SecondCannon->Destroy();
			}
			SecondCannon = SpawnCannon(CannonClass, SecondCannonPivotComponent);
			break;
		}
	}
}

void ATGPlayerPawn::RefillAmmoAllCannons(int count)
{
	MainCannon->RefillAmmo(count);
	SecondCannon->RefillAmmo(count);
}

void ATGPlayerPawn::StartMainFire()
{
	if (MainCannon != nullptr)
	{
		MainCannon->StartFire();
	}
	else
	{
		UE_LOG(LogTG, Error, TEXT("MainCannon is NULL!"));
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("MainCannon is NULL!"));
	}
}

void ATGPlayerPawn::StopMainFire() const
{
	if (MainCannon != nullptr)
	{
		MainCannon->StopFire();
	}
}

void ATGPlayerPawn::StartSecondFire()
{
	if (SecondCannon != nullptr)
	{
		SecondCannon->StartFire();
	}
	else
	{
		UE_LOG(LogTG, Error, TEXT("SecondCannon is NULL!"));
		GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("SecondCannon is NULL!"));
	}
}

void ATGPlayerPawn::StopSecondFire() const
{
	if (SecondCannon != nullptr)
	{
		SecondCannon->StopFire();
	}
}

