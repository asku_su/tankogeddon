#pragma once

#include "CoreMinimal.h"
#include "TGPlayerPawn.h"
#include "GameFramework/PlayerController.h"
#include "TGPlayerController.generated.h"

UCLASS()
class TANKOGEDDON_API ATGPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATGPlayerController();
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaSeconds) override;
	FVector GetMousePosition();

protected:
	UPROPERTY()
	ATGPlayerPawn* PlayerPawn;
	UPROPERTY()
	FVector MousePosition;

protected:
	virtual void BeginPlay() override;
	
	void OnMoveForvardInput(float AxisValue);
	void OnMoveRightInput(float AxisValue);
	void OnClickMainFire();
	void OnReleasedMainFire();
	void OnClickSecondFire();
	void OnReleasedSecondFire();
	
	void CalculateMousePositionNearPawn();
	
};
