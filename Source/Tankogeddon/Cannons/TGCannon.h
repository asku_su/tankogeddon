#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Tankogeddon/FilesHelpers/GameStructs.h"
#include "TGCannon.generated.h"

UCLASS()
class TANKOGEDDON_API ATGCannon : public AActor
{
	GENERATED_BODY()

public:
	ATGCannon();

protected:	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cannon Name")
	ECannonType CannonName = ECannonType::Unknown_Cannon; 

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	USceneComponent* TransformComponent; 
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* CannonMesh;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	UArrowComponent* ProjectileSpawnPoint;	

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	int NumberOfProjectile = 5;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	float FireRate = 1;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	float FireRange = 1000;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	float FireDamage = 1;

	FTimerHandle FireTimerHandle;

	bool bIsFire = false;
	int AvailableProjectiles;

	FString CannonNameString;
	
protected:
	virtual void BeginPlay() override;

	virtual void DoFire();

public:
	void StartFire();
	void StopFire();
	
	void RefillAmmo(int count = 0);

private:
	float TimeOfLastShot;
};
