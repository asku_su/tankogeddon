
#pragma once

#include "CoreMinimal.h"
#include "TGCannon.h"
#include "TGTraceCannon.generated.h"

UCLASS()
class TANKOGEDDON_API ATGTraceCannon : public ATGCannon
{
	GENERATED_BODY()

public:
	ATGTraceCannon();

protected:
	virtual void BeginPlay() override;

	virtual void DoFire() override;
	
public:
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	float TraceDistance = 1000.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	TEnumAsByte<ETraceTypeQuery> TraceChannel;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	FColor TraceColor = FColor::Green;	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	bool bDrawDebugTrace = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Fire params")
	float LineThickness = 0.5f;
};

