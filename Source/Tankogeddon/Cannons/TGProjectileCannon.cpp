

#include "TGProjectileCannon.h"


ATGProjectileCannon::ATGProjectileCannon()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATGProjectileCannon::DoFire()
{
	Super::DoFire();
	
	if (ProjectileClass)
	{
		if (bIsFire)
		{
			FActorSpawnParameters actorSpawnParameters;
			actorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			ATGProjectile* projectile = GetWorld()->SpawnActor<ATGProjectile>(ProjectileClass, ProjectileSpawnPoint->GetComponentLocation(), ProjectileSpawnPoint->GetComponentRotation(), actorSpawnParameters);
			GEngine->AddOnScreenDebugMessage(-1, 1.5f, FColor::Green, CannonNameString + FString::Printf(TEXT(" Fire - projectile %d/%d"), AvailableProjectiles, NumberOfProjectile));
			bIsFire = false;
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("ATGProjectileCannon:ProjectileClass is NULL!"));
	}
}


