

#include "TGTraceCannon.h"

#include "DrawDebugHelpers.h"
#include "Kismet/KismetSystemLibrary.h"


ATGTraceCannon::ATGTraceCannon()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATGTraceCannon::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATGTraceCannon::DoFire()
{
	Super::DoFire();
	if (bIsFire)
	{
		const FVector startTrace = ProjectileSpawnPoint->GetComponentLocation();
		const FVector endTrace = startTrace + ProjectileSpawnPoint->GetForwardVector() * TraceDistance;

		const EDrawDebugTrace::Type drawDebugType = bDrawDebugTrace ? EDrawDebugTrace::ForDuration : EDrawDebugTrace::None;

		FHitResult hitResult;
		if (UKismetSystemLibrary::LineTraceSingle(this, startTrace, endTrace, TraceChannel, false, {}, drawDebugType, hitResult, true))
		{
			DrawDebugLine(GetWorld(), startTrace, hitResult.Location, TraceColor, false, 0.5f, 0, LineThickness);
			if (hitResult.Actor.Get())
			{
				hitResult.Actor.Get()->Destroy();
			}
		}
		else
		{
			DrawDebugLine(GetWorld(), startTrace, endTrace, TraceColor, false, 0.5f, 0, LineThickness);
		}
		GEngine->AddOnScreenDebugMessage(-1, 1.5f, FColor::Cyan, CannonNameString + FString::Printf(TEXT(" Fire - projectile %d/%d"), AvailableProjectiles, NumberOfProjectile));
		bIsFire = false;
	}
}

void ATGTraceCannon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

