#include "TGCannon.h"

#include "Tankogeddon/Player/TGPlayerPawn.h"


ATGCannon::ATGCannon()
{
	PrimaryActorTick.bCanEverTick = false;
		
	TransformComponent = CreateDefaultSubobject<USceneComponent>("TransformComponent");
	SetRootComponent(TransformComponent);
	
	CannonMesh = CreateDefaultSubobject<UStaticMeshComponent>("CannonMesh");
	CannonMesh->SetupAttachment(TransformComponent);

	ProjectileSpawnPoint = CreateDefaultSubobject<UArrowComponent>("ProjectileSpqwnPoint");
	ProjectileSpawnPoint->SetupAttachment(CannonMesh);
}

void ATGCannon::BeginPlay()
{
	Super::BeginPlay();

	CannonNameString = EnumToString(CannonName);
	TimeOfLastShot =  -10.f;
	
	RefillAmmo();
	
}

void ATGCannon::DoFire()
{
	if (AvailableProjectiles <= 0)
	{
		GEngine->AddOnScreenDebugMessage(-1, 1.5f, FColor::Red, CannonNameString + FString::Printf(TEXT(" Out of projectile %d/%d"), AvailableProjectiles, NumberOfProjectile));
		bIsFire = false;
		return;
	}
	AvailableProjectiles -= 1;
	
	TimeOfLastShot =  GetWorld()->GetTimeSeconds();
	bIsFire = true;
}

void ATGCannon::StartFire()
{	
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("TimeOfLastShot %.5f Current Time %.5f"), TimeOfLastShot, GetWorld()->GetTimeSeconds()));
	float lastShotDelay = 1/FireRate - (GetWorld()->GetTimeSeconds() - TimeOfLastShot);
	lastShotDelay = lastShotDelay < 0 ? 0 : lastShotDelay;
	GetWorld()->GetTimerManager().SetTimer(FireTimerHandle, this, &ATGCannon::DoFire, 1/FireRate, true, lastShotDelay);	
}

void ATGCannon::StopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(FireTimerHandle);
}

void ATGCannon::RefillAmmo(int count)
{
	AvailableProjectiles += count > 0 ? count : NumberOfProjectile;
	if (AvailableProjectiles > NumberOfProjectile)
	{
		AvailableProjectiles = NumberOfProjectile;
	}
}
