
#pragma once

#include "CoreMinimal.h"
#include "TGCannon.h"
#include "Tankogeddon/Projectiles/TGProjectile.h"
#include "TGProjectileCannon.generated.h"

UCLASS()
class TANKOGEDDON_API ATGProjectileCannon : public ATGCannon
{
	GENERATED_BODY()

public:
	ATGProjectileCannon();

protected:
	virtual void DoFire() override;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
	TSubclassOf<ATGProjectile> ProjectileClass;
};
