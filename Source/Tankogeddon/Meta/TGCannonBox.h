
#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "Tankogeddon/Cannons/TGCannon.h"
#include "TGCannonBox.generated.h"

UCLASS()
class TANKOGEDDON_API ATGCannonBox : public AActor
{
	GENERATED_BODY()

public:
	ATGCannonBox();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	UBoxComponent* BoxComponent;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	TSubclassOf<ATGCannon> CannonClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	ECannonType CannonType = ECannonType::Unknown_Cannon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
	int CountOfShells = 0; 

protected:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
