

#include "TGCannonBox.h"

#include "Tankogeddon/Player/TGPlayerPawn.h"


ATGCannonBox::ATGCannonBox()
{
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>("BoxComponent");
	SetRootComponent(BoxComponent);
	
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATGCannonBox::OnBeginOverlap);
	
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	MeshComponent->SetupAttachment(BoxComponent);
}

void ATGCannonBox::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATGCannonBox::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	const FHitResult& SweepResult)
{
	
	if (auto pawn = Cast<ATGPlayerPawn>(OtherActor))
	{
		if (CannonClass)
		{
			pawn->SetCannon(CannonClass, CannonType, CountOfShells);
		}
		else
		{
			pawn->RefillAmmoAllCannons(CountOfShells);
		}
	}
}

