// Copyright Epic Games, Inc. All Rights Reserved.

#include "Tankogeddon.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Tankogeddon, "Tankogeddon" );

DEFINE_LOG_CATEGORY(LogTG);