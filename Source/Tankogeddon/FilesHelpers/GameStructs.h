#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "GameStructs.generated.h"


UENUM(BlueprintType)
enum class ECannonType : uint8
{
	Unknown_Cannon = 0	UMETA(DisplayName = "Unknown cannon"),
	Main_Cannon = 1		UMETA(DisplayName = "Main cannon"),
	Second_Cannon = 2	UMETA(DisplayName = "Second cannon")
};

/**
  * Convert the value of an enum to a string.
  *
  * @param EnumValue
  *    The enumerated type value to convert to a string.
  *
  * @return
  *    The key/name that corresponds to the value in the enumerated type.
  */
template<typename T>
FString EnumToString(const T EnumValue)
{
	FString Name = StaticEnum<T>()->GetNameStringByValue(static_cast<__underlying_type(T)>(EnumValue));
 
	check(Name.Len() != 0);
 
	return Name;
}